package me.rhon.hellokotlin.services

import me.rhon.hellokotlin.models.Match

data class MatchResponse(
        val events : List<Match>
)