package me.rhon.hellokotlin.services

import me.rhon.hellokotlin.models.Team

data class TeamResponse(
        val teams: List<Team>)