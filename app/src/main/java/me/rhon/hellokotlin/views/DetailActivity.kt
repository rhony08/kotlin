package me.rhon.hellokotlin.views

import android.annotation.TargetApi
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import org.jetbrains.anko.*

class DetailActivity : AppCompatActivity(){
    private lateinit var imgView : ImageView
    private lateinit var nameView : TextView
    private lateinit var descView : TextView

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        verticalLayout{
            padding=dip(16)
            horizontalGravity= Gravity.CENTER_HORIZONTAL
            imgView = imageView({
                topPadding=dip(10)
            }).lparams(LinearLayout.LayoutParams(dip(120), dip(120)))
            nameView = textView({
                topPadding=dip(10)
                textSize=24f
                textAlignment= View.TEXT_ALIGNMENT_CENTER
            }).lparams(LinearLayout.LayoutParams(matchParent, wrapContent))
            descView = textView({
                topPadding=dip(10)
                textSize=16f
                textAlignment= View.TEXT_ALIGNMENT_CENTER
            }).lparams(LinearLayout.LayoutParams(matchParent, wrapContent))
        }

//        val intent = intent
//        var item = intent.getParcelableExtra<Match>("match")
//        item.teamBadge?.let { Glide.with(this).load(it).into(imgView) }
//        item.awayScore?.let { nameView.text = it }
//        item.awayTeam?.let { descView.text = it }
    }
}