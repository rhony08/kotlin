package me.rhon.hellokotlin.views

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.widget.*
import me.rhon.hellokotlin.R
import org.jetbrains.anko.*
import org.jetbrains.anko.design.tabLayout
import org.jetbrains.anko.support.v4.viewPager

class MainActivity : AppCompatActivity() {
    private lateinit var tabL : TabLayout
    private lateinit var pager : ViewPager
//    private lateinit var tab: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        linearLayout {
            lparams(width = matchParent, height = matchParent)
            orientation = LinearLayout.VERTICAL
            tabL = tabLayout {
                lparams(width = matchParent, height = wrapContent)
            }
            pager = viewPager({
                id = R.id.viewPager
            }).lparams(width = matchParent, height = matchParent)
        }

        pager.adapter = PagerAdapter(supportFragmentManager)
        tabL.setupWithViewPager(pager)
    }

    class PagerAdapter(fm : FragmentManager) : FragmentPagerAdapter(fm) {
        private val NUM_ITEMS = 2
        override fun getItem(p0: Int): Fragment {
            if (p0 == 0) {
                return MatchFragment()
            }else {
                return MatchFragment()
            }
        }

        override fun getCount(): Int {
            return NUM_ITEMS
        }

        override fun getPageTitle(position: Int): CharSequence? {
            if (position == 0) return "Last Match"
            else return "Next Match"
        }
    }
}
