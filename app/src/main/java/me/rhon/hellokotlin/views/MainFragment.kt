package me.rhon.hellokotlin.views

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ProgressBar
import com.google.gson.Gson
import me.rhon.hellokotlin.R
import me.rhon.hellokotlin.adapters.HandlerTeam
import me.rhon.hellokotlin.adapters.MainAdapter
import me.rhon.hellokotlin.adapters.MatchAdapter
import me.rhon.hellokotlin.models.Match
import me.rhon.hellokotlin.models.Team
import me.rhon.hellokotlin.services.ApiRepository
import me.rhon.hellokotlin.services.TheSportDBApi
import me.rhon.hellokotlin.utils.MainPresenter
import me.rhon.hellokotlin.utils.MainView
import me.rhon.hellokotlin.utils.invisible
import me.rhon.hellokotlin.utils.visible
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.support.v4.swipeRefreshLayout

/**
 * Created by CHEVALIER-11 on 03-Nov-18.
 */
class MainFragment : AnkoComponent<ViewGroup?>, MainView, HandlerTeam {
    private lateinit var ctx : Context
    private lateinit var listMatch: RecyclerView
    private lateinit var progressBar: ProgressBar
    private lateinit var swipeRefresh: SwipeRefreshLayout

    private var matches: MutableList<Match> = mutableListOf()
    private var teams: MutableList<Team> = mutableListOf()
    private var key: MutableList<String?> = mutableListOf()
    private lateinit var presenter: MainPresenter
    private lateinit var adapter: MatchAdapter

    override fun createView(ui: AnkoContext<ViewGroup?>): View {
        this.ctx = ui.ctx
        val view =  with(ui) {
            linearLayout {
                lparams(width = matchParent, height = wrapContent)
                orientation = LinearLayout.VERTICAL
                topPadding = dip(16)
                leftPadding = dip(16)
                rightPadding = dip(16)

                swipeRefresh = swipeRefreshLayout {
                    setColorSchemeResources(R.color.colorAccent,
                            android.R.color.holo_green_light,
                            android.R.color.holo_orange_light,
                            android.R.color.holo_red_light)

                    relativeLayout {
                        lparams(width = matchParent, height = wrapContent)

                        listMatch = recyclerView {
                            lparams(width = matchParent, height = wrapContent)
                            layoutManager = LinearLayoutManager(ctx)
                        }

                        progressBar = progressBar {
                            visibility = View.INVISIBLE
                        }.lparams {
                            centerHorizontally()
                        }
                    }
                }
            }
        }

        adapter = MatchAdapter(matches, this)
        listMatch.adapter = adapter

        val request = ApiRepository()
        val gson = Gson()
        presenter = MainPresenter(this, request, gson)
        presenter.getTeamList()
        swipeRefresh.onRefresh {
            presenter.getMatchList()
        }
        return view
    }

    override fun showLoading() {
        progressBar.visible()
    }

    override fun hideLoading() {
        progressBar.invisible()
    }

    override fun showMatchList(data: List<Match>) {
        swipeRefresh.isRefreshing = false
        if (data.size > 0) {
            matches.clear()
            matches.addAll(data)
            adapter.notifyDataSetChanged()
        }
    }

    override fun showTeamList(data: List<Team>) {
        teams.clear()
        teams.addAll(data)
        key.clear()
        teams.forEach {
            if (it.teamId != null) key.add(it.teamId)
        }
    }

    override fun nameTeam(idTeam: String?): String? {
        return teams.get(key.indexOf(idTeam)).teamName
    }
}