package me.rhon.hellokotlin.adapters

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import me.rhon.hellokotlin.R
import me.rhon.hellokotlin.models.Match
import org.jetbrains.anko.*

/**
 * Created by CHEVALIER-11 on 02-Nov-18.
 */
class MatchAdapter (private val matches: List<Match>, private val handlerTeam: HandlerTeam)
    : RecyclerView.Adapter<MatchViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MatchViewHolder {
        return MatchViewHolder(MatchUI().createView(AnkoContext.create(parent.context, parent)))
    }

    override fun getItemCount(): Int = matches.size

    override fun onBindViewHolder(holder: MatchViewHolder, position: Int) {
        val match = matches[position]
        holder.bindItem(match, match.homeTeam, match.awayTeam)
    }
}
class MatchViewHolder(view: View) : RecyclerView.ViewHolder(view){
    private val time: TextView = view.find(R.id.time)
    private val home_team: TextView = view.find(R.id.home_team)
    private val away_team: TextView = view.find(R.id.away_name)
    private val score: TextView = view.find(R.id.score)

    fun bindItem(match: Match, homeTeam: String?, awayTeam: String?) {
        time.text = match.dateEvent
        val teamName = match.eventName?.split(" vs ")
        teamName?.let {
            home_team.text = it[0]
            away_team.text = it[1]
        }
        match.homeScore?.let {
            score.text = match.homeScore + " vs " + match.awayScore
        }
    }
}

class MatchUI : AnkoComponent<ViewGroup> {
    override fun createView(ui: AnkoContext<ViewGroup>): View {
        return with(ui) {
            linearLayout {
                lparams(width = matchParent, height = wrapContent)
                padding = dip(16)
                orientation = LinearLayout.VERTICAL
                textView {
                    id = R.id.time
                    textSize = 16f
                }.lparams{
                    margin = dip(16)
                }
                linearLayout {
                    lparams(width = matchParent, height = wrapContent)
                    textView {
                        id = R.id.home_team
                        textSize = 12f
                    }.lparams(width = dip(0), height = wrapContent){
                        weight = 4f
                    }
                    textView {
                        id = R.id.score
                        textSize = 16f
                        text = "VS"
                    }.lparams(width = dip(0), height = wrapContent){
                        margin = dip(8)
                        weight = 2f
                    }
                    textView {
                        id = R.id.away_name
                        textSize = 12f
                    }.lparams(width = dip(0), height = wrapContent){
                        weight = 4f
                    }
                }
            }
        }
    }
}

interface HandlerTeam{
    fun nameTeam(idTeam: String?): String?
}