package me.rhon.hellokotlin.utils

import me.rhon.hellokotlin.models.Match
import me.rhon.hellokotlin.models.Team

interface MainView {
    fun showLoading()
    fun hideLoading()
    fun showMatchList(data: List<Match>)
    fun showTeamList(data: List<Team>)
}